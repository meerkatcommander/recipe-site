import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { RecipeHeaderComponent } from './recipe-header/recipe-header.component';
import { RecipesParentComponent } from './recipes-parent/recipes-parent.component';
import { RecipesListComponent } from './recipes-parent/recipes-list/recipes-list.component';
import { RecipeDetailComponent } from './recipes-parent/recipe-detail/recipe-detail.component';
import { RecipeItemComponent } from './recipes-parent/recipes-list/recipe-item/recipe-item.component';
import { ShoppingListParentComponent } from './shopping-list-parent/shopping-list-parent.component';
import { ShoppingEditComponent } from './shopping-list-parent/shopping-edit/shopping-edit.component';
import { ShoppingListService } from './shopping-list-parent/shopping-list.service';
import { DropdownDirective } from './shared/dropdown.directive';


@NgModule({
  declarations: [
    AppComponent,
    RecipeHeaderComponent,
    RecipesParentComponent,
    RecipesListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    ShoppingListParentComponent,
    ShoppingEditComponent,
    DropdownDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [ShoppingListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
