import { Ingredient } from '../shared/ingredient.model'
import { EventEmitter } from '@angular/core';

export class ShoppingListService {
    ingredientsChanged = new EventEmitter<Ingredient[]>();
    ingredients:Ingredient[] = [
    new Ingredient("Box of Spaghetti", 1),
    new Ingredient("Can of Sauce", 1)
    ];

    getIngredients(){
        return this.ingredients.slice();
    }

    addIngredient(ingredient: Ingredient){
        this.ingredients.push(ingredient);
        this.ingredientsChanged.emit(this.ingredients);
    }

    addIngredients(ingredientsAdd: Ingredient[]){

        for (let ing in ingredientsAdd){
            if (this.ingredients.includes(ingredientsAdd[ing])){
                console.log(ingredientsAdd[ing].name);
                this.ingredients[ing].amount += ingredientsAdd[ing].amount;
            }
            else{
                this.ingredients.push(ingredientsAdd[ing])
            }
        }

        this.ingredientsChanged.emit(this.ingredients.slice());
    }

}