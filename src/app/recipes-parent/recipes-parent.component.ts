import { Component, OnInit } from '@angular/core';

import { RecipeService } from './recipe.service';

@Component({
  selector: 'app-recipes-parent',
  templateUrl: './recipes-parent.component.html',
  styleUrls: ['./recipes-parent.component.css'],
  providers: [RecipeService]
})
export class RecipesParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
