import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list-parent/shopping-list.service';

@Injectable()
export class RecipeService {

  private recipes: Recipe[] = [
    new Recipe('Spaghetti', 'Noodles with Tomato Sauce', 'https://upload.wikimedia.org/wikipedia/commons/2/2a/Spaghetti_al_Pomodoro.JPG',
  [
    new Ingredient('Spaghetti', 1),
    new Ingredient('Tomato Sauce', 1),
  ]),
    new Recipe('Burger', 'Beef patty on bun with veggies', 'https://upload.wikimedia.org/wikipedia/commons/1/11/Umami_Burger_hamburger.jpg',
  [
    new Ingredient('Bun', 1),
    new Ingredient('Beff Patty', 1),
    new Ingredient('Tomato', 2),
    new Ingredient('Cheese Slice', 1),
    new Ingredient('Lettuce', 1),
  ]),
    new Recipe('Burrito', 'The ultimate in cuisine', 'https://static.pexels.com/photos/461198/pexels-photo-461198.jpeg',
  [
    new Ingredient('Tortilla', 1),
    new Ingredient('Beans', 1),
    new Ingredient('Rice', 1),
    new Ingredient('Carnitas', 1),
    new Ingredient('Guacamole', 2),
  ])

  ];

  constructor(private slService: ShoppingListService){}

  getRecipes(){
      return this.recipes.slice();
  }

  getRecipe(id: number){
    return this.recipes[id];
  }

  addIndToShopList(ingredients: Ingredient[]){
    this.slService.addIngredients(ingredients);
  }




}